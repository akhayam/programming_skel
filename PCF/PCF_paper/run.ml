#load "pcf.cmo";;
open Pcf

exception Fail of string

module SMap = Map.Make(String)

module M:MONAD = struct
exception Branch_fail of string
	type 'a t = 'a
	let ret x = x
	let rec branch l =
		begin match l with
		| [] -> raise (Branch_fail "No branch matches")
		| b1 :: bq ->
				try b1 () with Branch_fail _ -> branch bq
		end
	let fail s = raise (Branch_fail s)
	let bind x f = f x
	let apply f x = f x
	let extract x = x

end



module rec Types : sig
  type nat = int
  type var = string
  type env = Unspec(M)(Types).value SMap.t
end = Types

module Unspec = struct
  include Unspec(M)(Types)
  let add ((a,b) : (nat*nat)) : nat M.t = M.ret (a + b)
  let sub ((a,b) : (nat*nat)) : nat M.t = M.ret (a - b)
  let div ((a,b) : (nat*nat)) : nat M.t = M.ret (a / b)
  let mul ((a,b) : (nat*nat)) : nat M.t = M.ret (a * b)
  let zero (a:nat) : unit M.t = match a with | 0 -> M.ret () | _ -> M.fail "not zero"
  let not_zero (a:nat) : unit M.t = match a with | 0 -> M.fail "is zero" | _ -> M.ret ()
  let extEnv (e:env) = M.ret (fun x -> M.ret (fun v -> M.ret (SMap.add x v e)))
  let getEnv (e:env) = M.ret (fun x -> M.ret (match SMap.find_opt x e with
                                           | Some v -> v
                                           | None -> failwith "the variable is not bound in the environment" ))

end

module Interp = MakeInterpreter(Unspec)

open Unspec
open Interp

let initial_env =  SMap.empty

let fact v =  LetIn ("fact",
                 Fix ("factrec",
                     "x",
                     If0(Var "x",
                         Const 1,
                         Mul (Var "x",App(Var "factrec", Sub (Var "x", Const 1)))
                       )
                   ),
                 App (Var "fact", v)
              )

let run t  = M.extract (M.extract (eval initial_env) t)

let _ = run (fact (Const 5))
