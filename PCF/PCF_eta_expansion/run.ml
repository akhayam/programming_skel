#load "pcf.cmo";;
open Pcf

exception Fail of string

module SMap = Map.Make(String)

module M:MONAD = struct
	type kind = | Anon | Spec of string | Unspec of string
	type 'b fcont = string -> 'b
	type ('a,'b) cont = 'a -> 'b fcont -> 'b
	type 'a t = {cont: 'b. (('a,'b) cont -> 'b fcont -> 'b)}
	let ret (x: 'a) = { cont= fun k fcont -> k x fcont }
	let bind (x: 'a t) (f: 'a -> 'b t) : 'b t =
		{ cont = fun k fcont -> x.cont (fun v fcont' -> (f v).cont k fcont') fcont }
	let fail s = { cont = fun k fcont -> fcont s }
	let rec branch l = { cont = fun k fcont ->
		match l with
		| [] -> fcont "No branch matches"
		| b :: bs -> (b ()).cont k (fun _ -> (branch bs).cont k fcont) }
	let apply _ f x = f x
	let extract x =
		x.cont (fun a _ -> a) (fun s -> failwith s)
end



module rec Types : sig
  type nat = int
  type var = string
  type env = Unspec(M)(Types).value SMap.t
end = Types

module Unspec = struct
  include Unspec(M)(Types)
  let add ((a,b) : (nat*nat)) : nat M.t = M.ret (a + b)
  let sub ((a,b) : (nat*nat)) : nat M.t = M.ret (a - b)
  let div ((a,b) : (nat*nat)) : nat M.t = M.ret (a / b)
  let mul ((a,b) : (nat*nat)) : nat M.t = M.ret (a * b)
  let zero (a:nat) : unit M.t = match a with | 0 -> M.ret () | _ -> M.fail "not zero"
  let not_zero (a:nat) : unit M.t = match a with | 0 -> M.fail "is zero" | _ -> M.ret ()
  let extEnv (e:env) = M.ret (fun x -> M.ret (fun v -> M.ret (SMap.add x v e)))
  let getEnv (e:env) = M.ret (fun x -> M.ret (match SMap.find_opt x e with
                                           | Some v -> (Ok v)
                                           | None -> Exc ))
  let fresh (x:var) : var M.t = let v = Random.int(999999) in M.ret(x^string_of_int(v))

end

module Interp = MakeInterpreter(Unspec)

open Unspec
open Interp

let initial_env =  SMap.empty

let fact v =  LetIn ("fact",
                 Fix ("factrec",
                      Lam ( "x",
                            If0(Var "x",
                               Const 1,
                               Mul (Var "x",App(Var "factrec", Sub (Var "x", Const 1)))
                              )
                        )
                      ),
                 App (Var "fact", v)
              )

let run t  = M.extract (M.extract (eval initial_env) t)

let _ = run (fact (Const 5))
