Implementation of the languages and the examples presented in the paper "A Practical Approach for Describing Language Semantics", submitted to "The Art, Science, and Engineering of Programming" journal.

# Installation
## Dependencies:

- necro
- necrolib
- necroml

## How to install the skel environment

```
opam switch create necro 4.09.1
eval $(opam env)
opam repository add necro https://gitlab.inria.fr/skeletons/opam-repository.git#necro
opam install necroml
```


# How to run
(*write the nicest tutorial ever*)
