type nat
type var
type env
type state
type loc

type value = | Nat nat | Clos (var,expr,env) | ClosRec (var,expr,env,var) | Loc loc | Unit | Susp(value,cont<out>) | Cont cont<out> | Env env

type expr =
| Const nat
| Add (expr,expr)
| Sub (expr,expr)
| Div (expr,expr)
| Mul (expr,expr)
| Try (expr,expr)
| Var var
| Lam (var,expr)
| App (expr,expr)
| LetIn (var,expr,expr)
| If0 (expr,expr,expr)
| Fix (var, var, expr)
| Seq (expr,expr)
| Skip
| Ref expr
| Get expr
| Set (expr,expr)
| Run expr
| Yield expr
| Handle (expr,(var,expr),(var,var,expr))
| Resume (expr,expr)

type out := st<exc<value>>
type cont<a> := a -> cstack<a> -> a
type cstack<a> =
| Nil
| Cons(cont<a>,cstack<a>)
type contM<a> := cont<a> -> cstack<a> -> a


type exc<a> = | Exc | Ok a
type reader<a> := env -> a
type st<a> := state -> (a,state)

val bind<a> (w:reader<contM<st<exc<a>>>>)(f:a → reader<contM<st<exc<a>>>>) : reader<contM<st<exc<a>>>> =
  λs:env ->
  λk:cont<st<exc<a>>> →
  λks:cstack<st<exc<a>>> →
  λm:state →
    w s (λvse:st<exc<a>> →
         λks1: cstack<st<exc<a>>> →
         λm1:state →
            let (ve,m2) = vse m1 in
            branch let Exc = ve in k (λm3:state → (Exc<a>,m3)) ks1 m2
            or     let Ok v = ve in
                   let w' = f v in w' s k ks1 m2
            end) ks m

val bind1<a> (w:contM<st<reader<exc<a>>>>)(f:a -> contM<st<reader<exc<a>>>>) : contM<st<reader<exc<a>>>> =
  λk: cont<st<reader<exc<a>>>> ->
  λks: cstack<st<reader<exc<a>>>> ->
  λm: state ->
    w (λvse:st<reader<exc<a>>> ->
       λks1: cstack<st<reader<exc<a>>>> ->
       λm1: state ->
          (λs: env ->
            let (ve,m2) = vse m1 in
            let v = ve s in
            branch let Exc = v in Exc<a>
            or     let Ok v' = v in
                   let w' = f v' in (*continuation*)
                   let (vc, _) = w' k ks1 m2 in (*result of the continuation*)
                   vc s (*final result of the continuation*)
            end, m1)) ks m


binder @ := bind

val exccont<a> (w:reader<contM<st<exc<a>>>>)(f:() → reader<contM<st<exc<a>>>>) : reader<contM<st<exc<a>>>> =
  λs:env ->
     λk:cont<st<exc<a>>> →
        λks:cstack<st<exc<a>>> →
           λm:state →
              w s (λvse:st<exc<a>> →
                       λks1: cstack<st<exc<a>>> →
                            λm1:state →
                                let (ve,m2) = vse m1 in
                                branch let Exc = ve in
                                       let w' = f () in w' s k ks1 m2
                                or     let Ok v = ve in k (λm3:state → (Ok<a> v,m3)) ks1 m2
                                end)
               ks m
binder ? := exccont

val ask : output =
  λs:env →
    λk:cont<out> ->
       λks:cstack<out> ->
          λm:state ->
            k (λm1:state -> (Ok<value> (Env s),m1)) ks m

val local (s:output) (f: () → output) : output =
  λs_p:env ->
      λk:cont<out> ->
         λks:cstack<out> ->
            λm:state ->
            s s_p (λvs:out ->
                      λks1:cstack<out> ->
                          λm1:state ->
                              let (Ok (Env s1),m2) = vs m1 in
                              let w = f () in w s1 k ks1 m2) ks m

val return<a> (v:a):reader<contM<st<exc<a>>>> =
  λ_:env ->
    λk:cont<st<exc<a>>> →
       λks:cstack<st<exc<a>>> →
           λm:state →
              k (λm1: state → (Ok<a> v,m1)) ks m

val ret (v:value) : output = return<value> v

val throw<a> (_:()):reader<contM<st<exc<a>>>> =
  λ_:env ->
    λk:cont<st<exc<a>>> →
       λks:cstack<st<exc<a>>> →
           λm:state →
              k (λm1: state → (Exc<a>,m1)) ks m

val thr (_:()) : output = throw<value> ()

val pushCont (_:()) (f: () → output) : output =
  λs:env ->
    λk:cont<out> →
     λks:cstack<out> → f () s id (Cons<out>(k,ks))

val alloc: state → value → (state, loc)
val m_r: state → loc → value
val m_w: state → loc → value → state

val newref (v: value) : output =
  λ_:env ->
    λk:cont<out> →
      λks:cstack<out> →
         let st = λs: state → let (s',l) = alloc s v in (Ok<value> (Loc l),s') in
         k st ks

val get (l: loc) : output =
  λ_:env ->
    λk:cont<out> →
      λks:cstack<out> →
    let st = λs: state → let v = m_r s l in (Ok<value> v,s) in
    k st ks

val set ((l,v):(loc,value)) : output =
  λ_:env ->
    λk:cont<out> →
      λks:cstack<out> →
    let st = λs: state → let s' = m_w s l v in (Ok<value> Unit,s') in
    k st ks

type output := reader<contM<st<exc<value>>>>

val add : (nat,nat) -> nat
val sub : (nat,nat) -> nat
val div : (nat,nat) -> nat
val mul : (nat,nat) -> nat

val zero : nat -> ()
val not_zero : nat -> ()

val getEnv : env -> var -> value

val extEnv : env -> var -> value -> env

val envGet (x:var) : output =
    let Env s =@ ask in
    let v = getEnv s x in ret v

val envExt ((x,v):(var,value)) : output =
    let Env s =@ ask in
    let s' = extEnv s x v in
    ret (Env s')

val makeClosure ((x,b):(var,expr)) : output =
    let Env s =@ ask in
    ret (Clos (x,b,s))

val makeRecursiveClosure ((x,b,f):(var,expr,var)) : output =
    let Env s =@ ask in
    ret (ClosRec (x,b,s,f))

val closureEnv (v:value) : output =
    branch let Clos (_,_,s) = v in ret (Env s)
    or     let ClosRec (_,_,s,_) = v in ret (Env s)
    or     let Nat _ = v in thr ()
    or     let Unit = v in thr ()
    or     let Loc _ = v in thr ()
    or     let Env _ = v in thr ()
    or     let Susp _ = v in thr ()
    or     let Cont _ = v in thr ()
    end

val eval (e:expr) : output =
    branch let Const n = e in ret (Nat n)
    or     let Add (e1,e2) = e in
           let Nat n1 =@ eval e1 in
           let Nat n2 =@ eval e2 in
           let n = add(n1,n2) in ret (Nat n)
    or     let Sub (e1,e2) = e in
           let Nat n1 =@ eval e1 in
           let Nat n2 =@ eval e2 in
           let n = sub(n1,n2) in ret (Nat n)
    or     let Div (e1,e2) = e in
           let Nat n1 =@ eval e1 in
           let Nat n2 =@ eval e2 in
           branch zero n2; thr()
           or     not_zero n2;
                  let n = div(n1,n2) in ret (Nat n)
           end
    or     let Mul (e1,e2) = e in
           let Nat n1 =@ eval e1 in
           let Nat n2 =@ eval e2 in
           let n = mul(n1,n2) in ret (Nat n)
    or     let Try (e1,e2) = e in
           eval e1;? eval e2
    or     let Var x = e in
           envGet x
    or     let Lam (x,eb) = e in
           makeClosure(x,eb)
    or     let App (e1,e2) = e in
           let cl =@ eval e1 in
           let w =@ eval e2 in
           branch let Clos (x,eb,_) = cl in
                  closureEnv cl;%local
                  envExt(x, w);%local
                  eval eb
           or     let ClosRec (x,eb,_,f) = cl in
                  closureEnv cl;%local
                  envExt(x, w);%local
                  envExt(f, cl);%local
                  eval eb
           end
    or     let LetIn (x,ex,ec) = e in
           let v =@ eval ex in
           envExt(x, v);%local
           eval ec
    or     let If0 (ec,e1,e2) = e in
           let Nat n =@ eval ec in
           branch zero n; eval e1
           or     not_zero n; eval e2
           end
    or     let Fix (f, x, ex) = e in
           makeRecursiveClosure(x,ex,f)
    or     let Seq (e1,e2) = e in
           eval e1;@
           eval e2
    or     let Skip = e in
           ret Unit
    or     let Ref es = e in
           let v =@ eval es in
           newref(v)
    or     let Get eg = e in
           let Loc l =@ eval eg in
           get(l)
    or     let Set (el,ev) = e in
           let Loc l =@ eval el in
           let v =@ eval ev in
           set(l,v)
    or     let Run e' = e in
           ();%pushCont
           eval e'
    or     let Yield e' = e in
           let v =@ eval e' in
           susp v
    or     let Handle (eh,(xvr,er),(xvc,xk,ec)) = e in
           let v =@ eval eh in
           branch let v = getPure v in
                  envExt(xvr, v);%local
                  eval er
           or     let (v,k) = getSusp v in
                  envExt(xvc, v);%local
                  envExt(xk, (Cont k));%local
                  eval ec
           end
    or     let Resume (ek,ev) = e in
           let kv =@ eval ek in
           let v =@ eval ev in
           let k = getCont kv in
           resume k v
    end

val id (f:out)(ks:cstack<out>):out =
  λ s:state →
  branch let Nil = ks in f s
  or     let Cons (k, ks') = ks in k f ks' s
  end

val susp (v:value):output =
  λ_:env ->
    λk:cont<out> →
      λks:cstack<out> →
          let Cons(k',ks') = ks in
          k' (λm:state → (Ok<value> (Susp (v, k)),m)) ks'

val resume (kv:cont<out>)(v:value):output =
  λ_:env ->
    λk:cont<out> →
      λks:cstack<out> →
           kv (λm: state → (Ok<value> v,m)) (Cons<out> (k, ks))

val getPure (v:value) : value =
    branch let Nat _ = v in v
    or     let Clos _ =  v in v
    or     let Unit = v in v
    or     let Loc _ = v in v
    end

val getSusp (v:value) : (value,cont<out>) =
    let Susp (v',k) = v in (v',k)

val getCont (v:value) : cont<out> =
    let Cont k = v in k

val init (e: expr) : env -> st<exc<value>> =
  λ s:env ->
      λ m:state ->
          let w = eval e in
          w s id Nil<out> m


