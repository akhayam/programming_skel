#load "yield.cmo";;
open Yield

exception Fail of string

module SMap = Map.Make(String)
module IMap = Map.Make(struct type t = int let compare = compare end)

module M:MONAD = struct
exception Branch_fail of string
	type 'a t = 'a
	let ret x = x
	let rec branch l =
		begin match l with
		| [] -> raise (Branch_fail "No branch matches")
		| b1 :: bq ->
				try b1 () with Branch_fail _ -> branch bq
		end
	let fail s = raise (Branch_fail s)
	let bind x f = f x
	let apply f x = f x
	let extract x = x

end

module rec Types : sig
  type nat = int
  type var = string
  type loc = int
  type env = Unspec(M)(Types).value SMap.t
  type state = (Unspec(M)(Types).value IMap.t) * int
end = Types

module Unspec = struct
  include Unspec(M)(Types)
  let alloc (h,i) = M.ret (fun v -> let h' = IMap.add i v h in M.ret ((h', i+1), i))
  let add ((a,b) : (nat*nat)) : nat M.t = M.ret (a + b)
  let sub ((a,b) : (nat*nat)) : nat M.t = M.ret (a - b)
  let div ((a,b) : (nat*nat)) : nat M.t = M.ret (a / b)
  let mul ((a,b) : (nat*nat)) : nat M.t = M.ret (a * b)
  let zero (a:nat) : unit M.t = match a with | 0 -> M.ret () | _ -> M.fail "not zero"
  let not_zero (a:nat) : unit M.t = match a with | 0 -> M.fail "is zero" | _ -> M.ret ()
  let extEnv (e:env) = M.ret (fun x -> M.ret (fun v -> M.ret (SMap.add x v e)))
  let getEnv (e:env) = M.ret (fun x -> (match SMap.find_opt x e with
                                        | Some v -> M.ret v
                                        | None -> M.fail "variable not bound" ))
  let m_r (h,i) = M.ret (fun l -> let v = IMap.find_opt l h in
                                      (match v with
                                      | Some v -> M.ret v
                                      | None -> M.fail "location not allocated"))
  let m_w (h,i) = M.ret (fun l -> M.ret (fun v -> let h' = IMap.add l v h in M.ret (h',i)))

end

module Interp = MakeInterpreter(Unspec)

open Unspec
open Interp

let initial_env =  SMap.empty
let initial_state = (IMap.empty, 0)


let run t =
  M.extract (M.extract ((M.extract (init t)) initial_env) initial_state)

let fact =  Fix ("factrec", "x",
                 If0(Var "x",
                     Const 1,
                     Mul (Var "x",App(Var "factrec", Sub (Var "x", Const 1)))
                   )
              )


let _ = run (App (fact,Const 5))

let figure8 =
        LetIn ("x",
               Run (Seq (Yield (Const 1),Seq (Yield (Const 2),Const 3))),
               Handle (Var "x",
                       ("_", Skip),
                       ("v1", "k1", Handle (Resume (Var "k1",Skip),
                                          ("v2", Var "v2"),
                                          ("v2", "k2",
                                           Handle (Resume (Var "k2", Skip),
                                                   ("v3", Add (Mul (Var "v1", Const 100),
                                                               Add (Mul (Var "v2", Const 10),
                                                                    Var "v3"))),
                                                       ("_","_", Skip)))))))

let _ = run figure8

let figure8_1 =
        LetIn ("x",
               Run (Seq (Yield (Const 1),Seq (Yield (Const 2),Const 3))),
               Handle (Var "x",
                       ("_",Skip),
                       ("v1", "k1",
                        Handle (Resume (Var "k1",Skip),
                                ("_", Skip),
                                ("v2", "k2",
                                 Handle (Resume (Var "k2", Skip),
                                         ("v3", App(fact,Var "v3")),
                                         ("_","_",Skip)))))))

let _ = run figure8_1

let recCL = LetIn ("d", Fix ("rec","_", Var "rec"), App (Var "d",Var "d"))
let _ = run recCL

let figure8_2 =
        LetIn ("x",
               Run (Seq (Yield (Const 1),Seq (Yield (Const 2), Const 3))),
               Handle (Var "x",
                       ("_",Skip),
                       ("v1","k1",
                        Handle (Resume (Var "k1",Skip),
                                ("_", Skip),
                                ("v2","_",App(fact,Var "v2"))))))

let _ = run figure8_2

let figure_9 =
          LetIn ("x", Run (LetIn ("y",
                                  (Const 1),
                                  LetIn ("z", Yield (Var "y"), Add(Mul (Var "y", Const 100),
                                                                   Add(Mul (Var "z", Const 10),
                                                                       Yield (Const 0)))))),
                 Handle (Var "x",
                         ("_", Skip),
                         ("v1", "k1",
                          Handle (Resume(Var "k1", Add (Var "v1", Const 1)),
                                  ("_", Skip),
                                  ("v2","k2",
                                   Handle (Resume (Var "k2", Const 3),
                                           ("v3", Var "v3"),
                                           ("_", "_", Skip)
                                           ))))))

let _ = run figure_9

(*communicating values back. each time we handle a suspension, we multiply the yield value by it's factorial result*)

let sum_eval = Run (Add(Const 1,Const 2))

let _ = run sum_eval

let sum_yield_eval = Run (Add (Yield (Const 1),Const 2))

let _ = run sum_yield_eval

let handle_sum_eval = Handle (Add(Run (Add(Const 1,Const 2)),Const 3),
                              ("v1", Var "v1"),
                              ("v1","_",Var "v1"))


let _ = run handle_sum_eval

let handle_sum_yield_eval = Handle (Run (Add (Yield (Const 1),Const 2)),
                              ("v1", Var "v1"),
                              ("v1","k1",
                               Handle ((Resume (Var "k1",Add (Var "v1",Const 3))),
                                       ("v2", Var "v2"),
                                       ("v2", "_",Var "v2")
                                       )))

let _ = run handle_sum_yield_eval

let figure_13 =
          LetIn ("x", Run (LetIn ("y", Ref (Const 1),
                                  LetIn ("x1", Get (Var "y"),
                                         LetIn ("x2", Get (Yield (Var "y")),
                                                Seq (Yield (Set (Var "y",Const 1)),
                                                     Add (Mul (Const 100, Var "x1"),
                                                          Add (Mul (Const 10, Var "x2"),
                                                               Get (Var "y")))))))),
                 Handle (Var "x",
                         ("v1", Var "v1"),
                         ("ref","k1",
                          Seq (Set (Var "ref", Const 2),
                               Handle (Resume(Var "k1", Var "ref"),
                                       ("v2", Var "v2"),
                                       ("_", "k2",
                                        Seq (Set (Var "ref", Add(Get (Var "ref"),Const 2)),
                                             Handle (Resume (Var "k2", Var "_"),
                                                     ("v3", Var "v3"),
                                                     ("v3", "_", Var "v3")))))))))
let _ = run figure_13

(*
type ('a,'b) f = 'a -> 'b;;
let x : ('a,'b) f = fun _ -> failwith "bottom";;
let couple : ('a,'b) f = fun x -> (x,x)
let id : ('a,'b) f = fun x : 'a -> x;;

type a = A;;
type b = B;;
type c = C;;
type d = D;;

let f : a -> b = fun _  -> B;;
let g : b -> c = fun _ -> C;;
let h : c -> d = fun _ -> D;;

g f A;;


x 3;;
id 3;;
((couple id) "ciao");;*)
