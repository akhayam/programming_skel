#load "arith_lan.cmo";;
open Arith_lan

exception Fail of string

module M:MONAD = struct
	type kind = | Anon | Spec of string | Unspec of string
	type 'b fcont = string -> 'b
	type ('a,'b) cont = 'a -> 'b fcont -> 'b
	type 'a t = {cont: 'b. (('a,'b) cont -> 'b fcont -> 'b)}
	let ret (x: 'a) = { cont= fun k fcont -> k x fcont }
	let bind (x: 'a t) (f: 'a -> 'b t) : 'b t =
		{ cont = fun k fcont -> x.cont (fun v fcont' -> (f v).cont k fcont') fcont }
	let fail s = { cont = fun k fcont -> fcont s }
	let rec branch l = { cont = fun k fcont ->
		match l with
		| [] -> fcont "No branch matches"
		| b :: bs -> (b ()).cont k (fun _ -> (branch bs).cont k fcont) }
	let apply _ f x = f x
	let extract x =
		x.cont (fun a _ -> a) (fun s -> failwith s)
end



module rec Types : sig
  type nat = int
end = Types

module Unspec = struct
  include Unspec(M)(Types)
  let add ((a,b) : (nat*nat)) : nat M.t = M.ret (a + b)
  let sub ((a,b) : (nat*nat)) : nat M.t = M.ret (a - b)
end

module Interp = MakeInterpreter(Unspec)

open Unspec
open Interp

let _ = M.extract (eval (Add (Const 1,Const 2)))
let _ = M.extract (eval (Sub (Const 1,Const 2)))
