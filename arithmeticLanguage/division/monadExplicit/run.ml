#load "arith_lan.cmo";;
open Arith_lan

exception Fail of string

module M:MONAD = struct
	type kind = | Anon | Spec of string | Unspec of string
	type 'b fcont = string -> 'b
	type ('a,'b) cont = 'a -> 'b fcont -> 'b
	type 'a t = {cont: 'b. (('a,'b) cont -> 'b fcont -> 'b)}
	let ret (x: 'a) = { cont= fun k fcont -> k x fcont }
	let bind (x: 'a t) (f: 'a -> 'b t) : 'b t =
		{ cont = fun k fcont -> x.cont (fun v fcont' -> (f v).cont k fcont') fcont }
	let fail s = { cont = fun k fcont -> fcont s }
	let rec branch l = { cont = fun k fcont ->
		match l with
		| [] -> fcont "No branch matches"
		| b :: bs -> (b ()).cont k (fun _ -> (branch bs).cont k fcont) }
	let apply _ f x = f x
	let extract x =
		x.cont (fun a _ -> a) (fun s -> failwith s)
end



module rec Types : sig
  type nat = int
end = Types

module Unspec = struct
  include Unspec(M)(Types)
  let add ((a,b) : (nat*nat)) : nat M.t = M.ret (a + b)
  let sub ((a,b) : (nat*nat)) : nat M.t = M.ret (a - b)
  let div ((a,b) : (nat*nat)) : nat M.t = M.ret (a / b)
  let mul ((a,b) : (nat*nat)) : nat M.t = M.ret (a * b)
  let zero (a:nat) : unit M.t = match a with | 0 -> M.ret () | _ -> M.fail "not zero"
  let not_zero (a:nat) : unit M.t = match a with | 0 -> M.fail "is zero" | _ -> M.ret ()
end

module Interp = MakeInterpreter(Unspec)

open Unspec
open Interp

let _ = M.extract (eval (Add (Const 1,Const 2)))
let _ = M.extract (eval (Sub (Const 1,Const 2)))
let _ = M.extract (eval (Div (Const 2,Const 2)))
let _ = M.extract (eval (Div (Const 2,Const 0)))
let _ = M.extract (eval (Mul (Const 2,Const 0)))
let _ = M.extract (eval (Mul (Const 0,Const 0)))
let _ = M.extract (eval (Try (Div (Const 2,Const 0),
                              Div (Const 84,Const 2))))
