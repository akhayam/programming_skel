open Necromonads

module type TYPE = sig
  type nat = int
end

module Spec(M:MONAD) = struct
  let add (a,b) = M.ret (a + b)
  let sub (a,b) = M.ret (a - b)
end
