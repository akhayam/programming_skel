open Arith

module Inst(M:MONAD) = struct

  module rec Types : sig
    include Arith_inst.TYPE
  end = Types

  module Spec = struct
    include Unspec(M)(Types)
    include Arith_inst.Spec(M)
  end

  module Interp = MakeInterpreter(Spec)

end

module InterpInst = Inst(Necromonads.ContPoly)

open InterpInst.Interp

let _ = match M.extract (eval (Add (Const 1,Const 2))) with
  | Nat n -> Printf.printf "%d\n" n
let _ = match M.extract (eval (Sub (Const 1,Const 2))) with
  | Nat n -> Printf.printf "%d\n" n
