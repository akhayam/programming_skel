open Pcf

module Inst(M:MONAD) = struct

  module rec Types : sig
    include Arith_inst.TYPE
    include Env_inst.TYPE with type value = Unspec(M)(Types).value
  end = Types

  module Spec = struct
    include Unspec(M)(Types)
    include Arith_inst.Spec(M)
    include Arith_div_inst.Spec(M)
    include Env_inst.Spec(M)
  end

  module Interp = MakeInterpreter(Spec)
end

module InterpInst = Inst(Necromonads.ContPoly)

open InterpInst.Interp

let fact v =  LetIn ("fact",
                 Fix ("factrec",
                     "x",
                     Ifz(Var "x",
                         Const 1,
                         Mul (Var "x",App(Var "factrec", Sub (Var "x", Const 1)))
                       )
                   ),
                 App (Var "fact", v)
              )

let run t  = M.extract (M.extract (eval t) initialEnv)

let _ = match run (fact (Const 5)) with
  | Ok (Nat n) -> Printf.printf "%d\n" n
  | _ -> assert false

let _ = match run (LetIn ("x", Const 1, Var "x")) with
  | Ok (Nat n) -> Printf.printf "%d\n" n
  | _ -> assert false
