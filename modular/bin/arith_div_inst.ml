open Necromonads

module Spec(M:MONAD) = struct
  let div (a,b) = M.ret (a / b)
  let mul (a,b) = M.ret (a * b)
  let zero a = match a with | 0 -> M.ret () | _ -> M.fail "not zero"
  let not_zero a = match a with | 0 -> M.fail "is zero" | _ -> M.ret ()
end
