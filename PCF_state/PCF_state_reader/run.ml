#load "pcf.cmo";;
open Pcf

exception Fail of string

module SMap = Map.Make(String)
module IMap = Map.Make(struct type t = int let compare = compare end)


module M:MONAD = struct
exception Branch_fail of string
	type 'a t = 'a
	let ret x = x
	let rec branch l =
		begin match l with
		| [] -> raise (Branch_fail "No branch matches")
		| b1 :: bq ->
				try b1 () with Branch_fail _ -> branch bq
		end
	let fail s = raise (Branch_fail s)
	let bind x f = f x
	let apply f x = f x
	let extract x = x

end



module rec Types : sig
  type nat = int
  type var = string
  type loc = int
  type env = Unspec(M)(Types).value SMap.t
  type state = (Unspec(M)(Types).value IMap.t) * int
end = Types

module Unspec = struct
  include Unspec(M)(Types)
  let alloc v = M.ret (fun s -> M.ret ( match s with
                                        | (h,i) -> let h' = IMap.add i v h in
                                                   (i,(h', i+1))))
  let add ((a,b) : (nat*nat)) : nat M.t = M.ret (a + b)
  let sub ((a,b) : (nat*nat)) : nat M.t = M.ret (a - b)
  let div ((a,b) : (nat*nat)) : nat M.t = M.ret (a / b)
  let mul ((a,b) : (nat*nat)) : nat M.t = M.ret (a * b)
  let zero (a:nat) : unit M.t = match a with | 0 -> M.ret () | _ -> M.fail "not zero"
  let not_zero (a:nat) : unit M.t = match a with | 0 -> M.fail "is zero" | _ -> M.ret ()
  let extEnv (e:env) = M.ret (fun x -> M.ret (fun v -> M.ret (SMap.add x v e)))
  let getEnv (e:env) = M.ret (fun x -> M.ret (match SMap.find_opt x e with
                                           | Some v -> v
                                           | None -> failwith "Variable not bound in the environment" ))
  let fresh (x:var) : var M.t = let v = Random.int(999999) in M.ret(x^string_of_int(v))
  let m_r l = M.ret (fun s ->
                  match s with
                  |(h,i) -> let v = IMap.find_opt l h in
                            (match v with
                             | Some v -> M.ret (v,s)
                             | None -> M.fail "Reference not found"))
  let m_w (l,v) = M.ret (fun s -> match s with
                                  | (h,i) -> let h' = IMap.add l v h in
                                             M.ret ((),(h',i)))

end

module Interp = MakeInterpreter(Unspec)

open Unspec
open Interp

let initial_env =  SMap.empty
let initial_state = (IMap.empty, 0)

let fact v =  LetIn ("v",
                     (Ref v),
                     LetIn ("fact",
                            Fix ("factrec",
                                 "r",
                                 If0(Get (Var "r"),
                                     Const 1,
                                     (Seq (Set (Var "r",Sub(Get(Var "r"), Const (1))),
                                           Mul (Add(Get(Var "r"),Const 1),
                                                App(Var "factrec", Var "v"))
                                        )
                                     )
                              )),
                            App (Var "fact", Var "v")
                ))



let run t  = M.extract (M.extract (M.extract (eval t) initial_env) initial_state)

let _ = run (fact (Const 5))
